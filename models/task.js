// Create the schema, model and export the file

	const mongoose = require("mongoose");

// create the schema using mongoose.Schema()

	const taskSchema = new mongoose.Schema({
		name: String,
		status: {
			type: {
				type: String,
				default: "Pending"
			}
		}
	});

	module.exports = mongoose.model("Task", taskSchema);