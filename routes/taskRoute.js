const express = require('express')

//Create a Router instance that functions as a middleware and routing system
const router = express.Router();

const taskController = require("../controllers/taskController");

//[Section] ROUTES 
//Route to get all the tasks 
//This route expects to receive a GET request at the URL "/tasks"

		router.get("/",(req,res)=>{
			//Invokes the 'getAllTasks' function from the "taksController.js"file and send the result back to teh client/Postman
			taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
		})

// [Route to create new task]
// This route expects to receive a POST request at the UTL "/tasks"

		router.post("/",(req,res)=>{
			// if info will be coming from the client, the data can be accessed from the request body
			taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
		})

// [Route to delete a task]
// This routes expecets to receive a DELETE request at the URL "/tasks/:id"
// the colon (:) is an identifier from the URL, it helps create a dynamic route which allows us to supply information in the url

		router.delete("/:id", (req,res) => {

			taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
		})


// [Route to update task]
// This task expects to receive a PUT request at the URL "/tasks/:id"

		router.put("/:id",(req,res) =>{

			taskController.updateTask(req.params.id, req.body).then(
				resultFromController => res.send(resultFromController))
		} )



//Use "module.exports" to export the router object to use in the app.js
module.exports = router;